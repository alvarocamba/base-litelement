import { LitElement, html } from 'lit-element'

class FichaPersona extends LitElement {
    
    static get properties () {
        return {
            name: {type: String},
            yearsInCompany: {type: Number},
            personInfo: {type: String},
            photo: {type: Object}
        }
    }

    // LitElement Lifecycle
    constructor () {

        super()

        this.name = "Prueba"
        this.yearsInCompany = 10
        this.updatePersonInfo()
    }

    render () {
        return html`
            <div>
                <label>Nombre completo</label>
                <input type="text" id="fname" value="${this.name}" @input="${this.updateName}"></input>
                <br />
                <label>Años en la empresa</label>
                <input type="text" name="yearsInCompany" value="${this.yearsInCompany}" @input="${this.updateYearsInCompany}"></input>
                <br />
                <input type="text" value="${this.personInfo}" disabled></input>
                <br />
            </div>
        `
    }

    updated (changedProperties) {
        console.log("-- ficha-persona > updated")
        // Buscamos cambios en una variable concreta
        if (changedProperties.has("name")) {
            console.log("-- Propiedad name valor inicial= " + changedProperties.get("name") + ", valor nuevo= " + this.name)
        }
        if (changedProperties.has("yearsInCompany")) {
            console.log("-- Propiedad yearsInCompany valor inicial= " + changedProperties.get("yearsInCompany") + " valor nuevo= " + this.yearsInCompany)
            this.updatePersonInfo()
        }
    }

    updateName (e) {
        console.log("-- ficha-persona > updateName")
        this.name = e.target.value
    }
    updateYearsInCompany (e) {
        console.log("-- ficha-persona > updateYearsInCompany")
        this.yearsInCompany = e.target.value
    }

    updatePersonInfo () {
        console.log("-- ficha-persona > updatePersonInfo")

        if (this.yearsInCompany >= 7) {
            this.personInfo = "lead"
        } else if (this.yearsInCompany >= 5) {
            this.personInfo = "senior"
        } else if (this.yearsInCompany >= 3) {
            this.personInfo = "team"
        } else {
            this.personInfo = "junior"
        }
    }
}

customElements.define('ficha-persona', FichaPersona)