import { LitElement, html } from 'lit-element'
import '../persona-ficha-listado/persona-ficha-listado.js'
import '../persona-form/persona-form.js'

class PersonaMain extends LitElement {

    static get properties () {
        return {
            people: {type: Array},
            showPersonForm: {type: Boolean}
        }
    }

    constructor () {
        super()

        this.showPersonForm = false
        this.people = [
            {
                name: "Batman",
                yearsInCompany: 5,
                profile: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ex mauris, convallis sed nisi quis, commodo bibendum mi. Nulla sodales libero quis pulvinar feugiat. Nunc lobortis mauris est, vehicula vestibulum dui suscipit vitae. Aenean rutrum, quam bibendum consequat porta, nunc est posuere orci, ut faucibus augue felis eget magna. Duis quis sodales ligula. Ut accumsan condimentum finibus. Etiam scelerisque vitae eros faucibus aliquet. Quisque ipsum ligula, maximus sit amet ex in, semper maximus turpis. Phasellus faucibus orci consequat ipsum pharetra, id ultricies mauris convallis. Sed augue ex, tincidunt sed lobortis eget, pulvinar eu urna. Nam nec tellus pretium, accumsan sapien at, accumsan lacus. Proin nibh tortor, vestibulum et tempor vitae, condimentum vitae dui.",
                photo: {
                    src: "./img/batman.png",
                    alt: "Batman"
                }
            },
            {
                name: "Deadpool",
                yearsInCompany: 6,
                profile: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ex mauris, convallis sed nisi quis, commodo bibendum mi. Nulla sodales libero quis pulvinar feugiat. Nunc lobortis mauris est, vehicula vestibulum dui suscipit vitae. Aenean rutrum, quam bibendum consequat porta, nunc est posuere orci, ut faucibus augue felis eget magna. Duis quis sodales ligula. Ut accumsan condimentum finibus. Etiam scelerisque vitae eros faucibus aliquet. Quisque ipsum ligula, maximus sit amet ex in, semper maximus turpis. Phasellus faucibus orci consequat ipsum pharetra, id ultricies mauris convallis. Sed augue ex, tincidunt sed lobortis eget, pulvinar eu urna. Nam nec tellus pretium, accumsan sapien at, accumsan lacus. Proin nibh tortor, vestibulum et tempor vitae, condimentum vitae dui.",
                photo: {
                    src: "./img/deadpool.png",
                    alt: "Deadpool"
                }
            },
            {
                name: "Spiderman",
                yearsInCompany: 7,
                profile: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ex mauris, convallis sed nisi quis, commodo bibendum mi. Nulla sodales libero quis pulvinar feugiat. Nunc lobortis mauris est, vehicula vestibulum dui suscipit vitae. Aenean rutrum, quam bibendum consequat porta, nunc est posuere orci, ut faucibus augue felis eget magna. Duis quis sodales ligula. Ut accumsan condimentum finibus. Etiam scelerisque vitae eros faucibus aliquet. Quisque ipsum ligula, maximus sit amet ex in, semper maximus turpis. Phasellus faucibus orci consequat ipsum pharetra, id ultricies mauris convallis. Sed augue ex, tincidunt sed lobortis eget, pulvinar eu urna. Nam nec tellus pretium, accumsan sapien at, accumsan lacus. Proin nibh tortor, vestibulum et tempor vitae, condimentum vitae dui.",
                photo: {
                    src: "./img/spiderman.png",
                    alt: "Spiderman"
                }
            },
            {
                name: "Superman",
                yearsInCompany: 9,
                profile: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ex mauris, convallis sed nisi quis, commodo bibendum mi. Nulla sodales libero quis pulvinar feugiat. Nunc lobortis mauris est, vehicula vestibulum dui suscipit vitae. Aenean rutrum, quam bibendum consequat porta, nunc est posuere orci, ut faucibus augue felis eget magna. Duis quis sodales ligula. Ut accumsan condimentum finibus. Etiam scelerisque vitae eros faucibus aliquet. Quisque ipsum ligula, maximus sit amet ex in, semper maximus turpis. Phasellus faucibus orci consequat ipsum pharetra, id ultricies mauris convallis. Sed augue ex, tincidunt sed lobortis eget, pulvinar eu urna. Nam nec tellus pretium, accumsan sapien at, accumsan lacus. Proin nibh tortor, vestibulum et tempor vitae, condimentum vitae dui.",
                photo: {
                    src: "./img/superman.png",
                    alt: "Superman"
                }
            },
            {
                name: "Thor",
                yearsInCompany: 11,
                profile: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ex mauris, convallis sed nisi quis, commodo bibendum mi. Nulla sodales libero quis pulvinar feugiat. Nunc lobortis mauris est, vehicula vestibulum dui suscipit vitae. Aenean rutrum, quam bibendum consequat porta, nunc est posuere orci, ut faucibus augue felis eget magna. Duis quis sodales ligula. Ut accumsan condimentum finibus. Etiam scelerisque vitae eros faucibus aliquet. Quisque ipsum ligula, maximus sit amet ex in, semper maximus turpis. Phasellus faucibus orci consequat ipsum pharetra, id ultricies mauris convallis. Sed augue ex, tincidunt sed lobortis eget, pulvinar eu urna. Nam nec tellus pretium, accumsan sapien at, accumsan lacus. Proin nibh tortor, vestibulum et tempor vitae, condimentum vitae dui.",
                photo: {
                    src: "./img/thor.png",
                    alt: "Thor"
                }
            },
            {
                name: "Lobezno",
                yearsInCompany: 18,
                profile: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ex mauris, convallis sed nisi quis, commodo bibendum mi. Nulla sodales libero quis pulvinar feugiat. Nunc lobortis mauris est, vehicula vestibulum dui suscipit vitae. Aenean rutrum, quam bibendum consequat porta, nunc est posuere orci, ut faucibus augue felis eget magna. Duis quis sodales ligula. Ut accumsan condimentum finibus. Etiam scelerisque vitae eros faucibus aliquet. Quisque ipsum ligula, maximus sit amet ex in, semper maximus turpis. Phasellus faucibus orci consequat ipsum pharetra, id ultricies mauris convallis. Sed augue ex, tincidunt sed lobortis eget, pulvinar eu urna. Nam nec tellus pretium, accumsan sapien at, accumsan lacus. Proin nibh tortor, vestibulum et tempor vitae, condimentum vitae dui.",
                photo: {
                    src: "./img/wolverine.png",
                    alt: "Lobezno"
                }
            }
        ]
    }

    render () {
        // NOTA: Los parametros que pasamos por etiqueta deben ser string siempre. Si es un object, se pone un punto delante
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <h2 class="text-center">Personas</h2>
            <div class="row" id="peopleList">
                <div class="row row-cols-1 row-cols-sm-4">
                    ${this.people.map(
                        person => html`
                            <persona-ficha-listado
                                fname="${person.name}"
                                yearsInCompany="${person.yearsInCompany}"
                                profile="${person.profile}"
                                .photo="${person.photo}"
                                @delete-person="${this.deletePerson}"
                                @info-person="${this.infoPerson}"
                            ></persona-ficha-listado>
                        `
                    )}
                </div>
            </div>
            <div class="row">
                <persona-form
                    @persona-form-close="${this.personFormClose}"
                    @persona-form-store="${this.personFormStore}"
                    class="d-none border rounded border-primary" id="personForm"></persona-form>
            </div>
        `
    }

    updated (changedProperties) {
        console.log("-- persona-main > updated")
        // Buscamos cambios en una variable concreta
        if (changedProperties.has("showPersonForm")) {
            
            if (this.showPersonForm === true) {
                this.showPersonFormData()
            } else {
                this.showPersonList()
            }            
        }

        if (changedProperties.has("people")) {

            this.dispatchEvent(new CustomEvent("updated-people", {
                    "detail": {
                        people: this.people
                    }
                }
            ))
        }
    }

    deletePerson (e) {
        console.log("-- persona-main > deletePerson")
        console.log("persona nombre= " + e.detail.name)
        
        this.people = this.people.filter(
            person => person.name != e.detail.name
        )
    }

    infoPerson (e) {
        console.log("-- persona-main > infoPerson")
        console.log("informacion persona= " + e.detail.name)

        let chosenPerson = this.people.filter(
            person => person.name === e.detail.name
        )
        console.log(chosenPerson)

        let personToShow = {}
        personToShow.name = chosenPerson[0].name
        personToShow.profile = chosenPerson[0].profile
        personToShow.yearsInCompany = chosenPerson[0].yearsInCompany

        this.shadowRoot.getElementById("personForm").person = personToShow
        this.shadowRoot.getElementById("personForm").editingPerson = true
        this.showPersonForm = true
    }

    showPersonFormData () {
        console.log("-- persona-main > showPersonFormData")

        this.shadowRoot.getElementById("personForm").classList.remove("d-none")
        this.shadowRoot.getElementById("peopleList").classList.add("d-none")
    }

    showPersonList () {
        console.log("-- persona-main > showPersonList")

        this.shadowRoot.getElementById("personForm").classList.add("d-none")
        this.shadowRoot.getElementById("peopleList").classList.remove("d-none")
    }

    personFormClose () {
        console.log("-- persona-main > personFormClose")

        this.showPersonForm = false
    }

    personFormStore (e) {
        console.log("-- persona-main > personFormStore")

        if (e.detail.editingPerson === true) {
            console.log("Actualizar person= " + e.detail.person.name)

            this.people = this.people.map(
                person => person.name === e.detail.person.name ? person = e.detail.person : person
            )
        } else {
            console.log("Guardar person= " + e.detail.person.name)
            this.people = [...this.people, e.detail.person]
        }
        this.showPersonForm = false
    }
}

customElements.define('persona-main', PersonaMain)