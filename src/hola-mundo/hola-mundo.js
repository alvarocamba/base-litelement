import { LitElement, html } from 'lit-element'

class HolaMundo extends LitElement {
    render () {
        // El render pinta lo que se visualiza por pantalla
        // La comilla ` permite presentar el html en modo multi-linea
        return html`
            <div>Hola Mundo!</div>
        `
    }
}

customElements.define('hola-mundo', HolaMundo)