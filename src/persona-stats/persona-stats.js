import { LitElement, html } from 'lit-element'

class PersonaStats extends LitElement {
    
    static get properties () {
        return {
            people: {type: Array}
        }
    }

    constructor () {
        super()

        this.people = []
    }

    updated (changedProperties) {

        if (changedProperties.has("people")) {

            let peopleStats = this.gatherPeopleArrayInfo(this.people)
            this.dispatchEvent(new CustomEvent("update-people-stats", {
                    "detail": peopleStats
                }
            ))
        }
    }

    gatherPeopleArrayInfo (people) {
        let peopleStats = {}
        peopleStats.numberOfPeople = people.length
        return peopleStats
    }
}

customElements.define('persona-stats', PersonaStats)