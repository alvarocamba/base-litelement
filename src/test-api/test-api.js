import { LitElement, html } from 'lit-element'

class TestApi extends LitElement {

    static get properties () {
        return {
            movies: {type: Array}
        }
    }

    constructor () {
        super ()

        this.movies = []
        this.getMovieData()
    }

    render () {
        return html`
            ${this.movies.map(
                movie => html`<div>La película ${movie.title}, fue dirigida por ${movie.director}</div>`
            )}
        `
    }

    getMovieData () {
        console.log("-- getMovieData")
        let xhr = new XMLHttpRequest()

        xhr.open("GET", "https://swapi.dev/api/films/")

        xhr.onload = () => {
            if (xhr.status === 200) {
                console.log("Petición completada correctamente")

                let APIResponse = JSON.parse(xhr.responseText)
                this.movies = APIResponse.results
            }
        }
        xhr.send()
        console.log("-- fin getMovieData")
    }
}

customElements.define('test-api', TestApi)