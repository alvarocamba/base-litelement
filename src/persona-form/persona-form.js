import { LitElement, html } from 'lit-element'

class PersonaForm extends LitElement {

    static get properties () {
        return {
            person: {type: Object},
            editingPerson: {type: Boolean}
        }
    }

    constructor () {
        super()

        this.resetFormData()
        this.editingPerson = false
    }

    render () {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <div>
                <form>
                    <div class="form-group">
                        <label>Nombre completo</label>
                        <input type="text" 
                            @input="${this.updateName}"
                            .value="${this.person.name}"
                            ?disabled="${this.editingPerson}"
                            class="form-control"
                            placeholder="Nombre completo"
                        />
                    </div>
                    <div class="form-group">
                        <label>Perfil</label>
                        <textarea @input="${this.updateProfile}" .value="${this.person.profile}" class="form-control" placeholder="Perfil" rows="5"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Años en la empresa</label>
                        <input type="text" @input="${this.updateYearsInCompany}" .value="${this.person.yearsInCompany}" class="form-control" placeholder="Años en la empresa" />
                    </div>
                    <button @click="${this.goBack}" class="btn btn-primary"><strong>Atrás</strong></button>
                    <button @click="${this.storePerson}" class="btn btn-success"><strong>Guardar</strong></button>
                </form>
            </div>
        `
    }

    updateName (e) {
        console.log("updateName")
        console.log("Actualizando la propiedad Name con valor " + e.target.value)

        this.person.name = e.target.value
    }

    updateProfile (e) {
        console.log("updateProfile")
        console.log("Actualizando la propiedad Profile con valor " + e.target.value)

        this.person.profile = e.target.value
    }

    updateYearsInCompany (e) {
        console.log("yearsInCompany")
        console.log("Actualizando la propiedad yearsInCompany con valor " + e.target.value)

        this.person.yearsInCompany = e.target.value
    }

    goBack (e) {
        console.log("goBack")
        e.preventDefault()

        this.dispatchEvent(new CustomEvent("persona-form-close", {}))
        this.resetFormData()
    }

    storePerson (e) {
        console.log("storePerson")
        e.preventDefault()

        this.person.photo = {
            src: "./img/persona1.png",
            alt: "Persona"
        }

        console.log("La propiedad name en person vale " + this.person.name)
        console.log("La propiedad Profile en person vale " + this.person.profile)
        console.log("La propiedad yearsInCompany en person vale " + this.person.yearsInCompany)

        this.dispatchEvent(new CustomEvent("persona-form-store", {
            "detail": {
                "person": {
                    "name": this.person.name,
                    "profile": this.person.profile,
                    "yearsInCompany": this.person.yearsInCompany,
                    "photo": this.person.photo
                },
                "editingPerson": this.editingPerson
            }
        }))
    }

    resetFormData () {
        this.person = {
            "name": "",
            "profile": "",
            "yearsInCompany": ""
        }
        this.editingPerson = false
    }
}

customElements.define('persona-form', PersonaForm)